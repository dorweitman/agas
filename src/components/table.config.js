const name = 'שם פרטי';
const lastname = 'שם משפחה';
const id = 'מספר אישי';
const age = 'גיל';

const person = {
    [name]: '',
    [lastname]: '',
    [id]: 0,
    [age]: 0,
};

module.exports = { person }; 